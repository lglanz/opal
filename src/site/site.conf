title = "OPAL - Getting Started"

// these folders are just copied to the output folder
folders = [ "articles", "artifacts", "images", "css", "fonts", "js" ]

// these resources are just copied to the output folder
resources = [ "OPALLogo.png", "favicon", "crossdomain.xml", "robots.txt" ]

// first transformed to HTML snippets and then combined with the default.template
pages = [
  { title = "OPAL", source = "index.md", useBanner = true },

  "Getting Started",
  { title = "Reading Class Files", source = "ReadingClassFiles.md" },
  { title = "Loading Java Projects", source = "Projects.md" },
  { title = "Using the Class Hierarchy", source = "ClassHierarchy.md" },
  { title = "Engineering Java Bytecode", source = "BytecodeEngineering.md", resources = ["BytecodeEngineeringCFG.svg"] },
  { title = "3-Address Code/SSA Code", source = "TAC.md" },

  "Tools",
  { title = "Supporting OPAL Developers", source = "DeveloperTools.md" },
  { title = "Exploring Bytecode", source = "JavaBytecodeExplorer.md" },
  { title = "Hermes", source = "Hermes.md", resources = ["Hermes.png"] },

  "Showcases",
  { title = "Detecting Useless Boxings", source = "UselessBoxing.snippet.html" },
  { title = "Package Dependencies in JDK8", source = "JDK8Dependencies.snippet.html", resources= ["JDK8DependenciesVisualization.html"]},
  { title = "Complexity of the JDK8", source = "JDK8Complexity.snippet.html", resources= ["JDK8Complexity.data.js"]},

  "Artifacts",
  { title = "Research Artifacts", source = "Artifacts.snippet.html", useBanner = true}
]
