org.opalj {

  br {

    reader {
      ClassFileReader {
        Java8LambdaExpressions {
          rewrite = true, // default is "true"
          logRewrites = false // default is "false"
        },
        deleteSynthesizedClassFilesAttributes = true // default is true
      }
    }

    analyses {
      PropertyStoreKey {
        debug = false // default is "false"
      }
    }

  }

  fpcf {

    registry {
      analyses.br = [
        {
          id = "MethodAccessibilityAnalysis",
          description = "Computes the project accessibility property of methods w.r.t. clients.",
          factory = "org.opalj.fpcf.analysis.MethodAccessibilityAnalysis"
        },
        {
          id = "FactoryMethodAnalysis",
          description = "Determines if a static method is an accessible factory method w.r.t. clients.",
          factory = "org.opalj.fpcf.analysis.FactoryMethodAnalysis"
        },
        {
          id = "InstantiabilityAnalysis",
          description = "Computes if a class can (possibly) be instantiated.",
          factory = "org.opalj.fpcf.analysis.SimpleInstantiabilityAnalysis"
        },
        {
          id = "CallableFromClassesInOtherPackagesAnalysis",
          description = "Computes whether a non-static method can be called via an super or subclass.",
          factory = "org.opalj.fpcf.analysis.CallableFromClassesInOtherPackagesAnalysis"
        },
        {
          id = "FieldMutabilityAnalysis",
          description = "Determines if fields are (effectively) final.",
          factory = "org.opalj.fpcf.analysis.FieldMutabilityAnalysis"
        },
        {
          id = "PurityAnalysis",
          description = "Determines if a method is pure (~ has no side effects).",
          factory = "org.opalj.fpcf.analysis.PurityAnalysis"
        },
        {
          id = "ClassExtensibilityAnalysis",
          description = "Determines which classes may have subclasses (this analysis is particularly relevant when analyzing libraries)",
          factory = "org.opalj.fpcf.analysis.ClassExtensibilityAnalysis"
        }
      ]
    }

    analysis {
      manager {
        debug = true // default is "false"
      }

      escape {
        debug = true // default is "false"
      }
    }
  }
}
