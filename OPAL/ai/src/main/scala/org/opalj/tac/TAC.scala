/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package tac

import org.opalj.io.writeAndOpen
import org.opalj.br.analyses.Project
import org.opalj.ai.domain
import org.opalj.ai.BaseAI
import org.opalj.ai.Domain
import org.opalj.ai.domain.RecordDefUse
import org.opalj.br.ClassFile
import org.opalj.br.Method
import org.opalj.log.GlobalLogContext
import org.opalj.log.ConsoleOPALLogger
import org.opalj.log.OPALLogger
import org.opalj.log.{Error ⇒ ErrorLogLevel}

/**
 * Creates the three-address representation for some method and prints it to std out.
 *
 * @example
 *         To convert all files of a project to TAC you can use:
 * {{{
 * import org.opalj.io.write
 * import org.opalj.tac._
 * import org.opalj.util.PerformanceEvaluation.time
 * val f = new java.io.File("/Users/eichberg/Downloads/presto-verifier-0.147-executable.zip")
 * val p = org.opalj.br.analyses.Project(f)
 * var i = 0
 * time {
 * p.parForeachMethodWithBody(parallelizationLevel=32){ mi =>
 *   val (code,_) = org.opalj.tac.AsQuadruples(mi.method,p.classHierarchy)
 *   val tac = ToTxt(code)
 *   val fileNamePrefix = mi.classFile.thisType.toJava+"."+mi.method.name
 *   val file = write(tac, fileNamePrefix, ".tac.txt")
 *   i+= 1
 *   println(i+":"+file)
 * }
 * }(t => println("Analysis time: "+t.toSeconds))
 * }}}
 *
 * @author Michael Eichberg
 */
object TAC {

    OPALLogger.updateLogger(GlobalLogContext, new ConsoleOPALLogger(true, ErrorLogLevel))

    def handleError(message: String): Nothing = {
        Console.err.println(error(message))
        sys.exit(-1)
    }

    def error(message: String): String = s"Error: $message \n$usage"

    def usage: String = {
        "Usage: java …TAC \n"+
            "-source <JAR file/Folder containing class files>\n"+
            "[-class <class file name> (filters the set of classes)]\n"+
            "[-method <method name/signature using Java notation>] (filters the set of methods)\n"+
            "[-naive (the naive representation is generated) | -domain <class name of the domain>]\n"+
            "[-cfg] (print control-flow graph)]\n"+
            "[-open (the generated representations will be written to disk and opened)]\n"+
            "Example:\n\tjava …TAC -jar /Library/jre/lib/rt.jar -class java.util.ArrayList .method toString"
    }

    def main(args: Array[String]): Unit = {

        // Parameters:
        var source: String = null
        var doOpen: Boolean = false
        var className: Option[String] = None
        var methodSignature: Option[String] = None
        var naive: Boolean = false
        var domainName: Option[String] = None
        var printCFG: Boolean = false

        // PARSING PARAMETERS
        var i = 0

        def readNextArg(): String = {
            i += 1
            if (i < args.length) {
                args(i)
            } else {
                handleError(s"missing argument: ${args(i - 1)}")
            }
        }

        while (i < args.length) {
            args(i) match {
                case "-naive" ⇒
                    naive = true
                    if (domainName.nonEmpty) handleError("-naive and -domain cannot be combined")

                case "-domain" ⇒
                    domainName = Some(readNextArg())
                    if (naive) handleError("-naive and -domain cannot be combined")

                case "-source" ⇒ source = readNextArg()
                case "-cfg"    ⇒ printCFG = true
                case "-open"   ⇒ doOpen = true
                case "-class"  ⇒ className = Some(readNextArg())
                case "-method" ⇒ methodSignature = Some(readNextArg())
                case unknown   ⇒ handleError(s"unknown parameter: $unknown")
            }
            i += 1
        }

        if (source == null) {
            handleError("missing parameters")
        }

        val project = Project(new java.io.File(source))
        if (project.projectMethodsCount == 0) {
            handleError(s"no methods found: $source")
        }

        val ch = project.classHierarchy
        for {
            cf ← project.allClassFiles
            if className.isEmpty || className.get == cf.thisType.toJava
        } {
            val methodsAsTAC = new StringBuilder()

            for {
                m ← cf.methods
                mSig = m.descriptor.toJava(m.name)
                if methodSignature.isEmpty || mSig.contains(methodSignature.get)
                code ← m.body
            } {
                val (tac: String, cfg: String) = if (naive) {
                    val (code, Some(cfg), _) =
                        TACNaive(m, ch, AllTACNaiveOptimizations, forceCFGCreation = true)
                    (ToTxt(code), tacToDot(code, cfg))
                } else {
                    val d: Domain with RecordDefUse =
                        if (domainName.isEmpty) {
                            new domain.l1.DefaultDomainWithCFGAndDefUse(project, cf, m)
                        } else {
                            // ... "org.opalj.ai.domain.l0.BaseDomainWithDefUse"
                            Class.
                                forName(domainName.get).asInstanceOf[Class[Domain with RecordDefUse]].
                                getConstructor(classOf[Project[_]], classOf[ClassFile], classOf[Method]).
                                newInstance(project, cf, m)
                        }
                    // val d = new domain.l0.BaseDomainWithDefUse(project, classFile, method)
                    val aiResult = BaseAI(cf, m, d)
                    val TACode(code, cfg, _, _) = TACAI(m, project.classHierarchy, aiResult)(Nil)
                    (ToTxt(code), tacToDot(code, cfg))
                }

                methodsAsTAC.append(mSig)
                methodsAsTAC.append("{\n")
                methodsAsTAC.append(tac)
                if (printCFG) {
                    if (doOpen) {
                        Console.println("wrote cfg to: "+writeAndOpen(cfg, m.toJava(cf), "cfg.gv"))
                    } else {
                        methodsAsTAC.append("\n/* - CFG")
                        methodsAsTAC.append(cfg)
                        methodsAsTAC.append("*/\n")
                    }
                }
                methodsAsTAC.append("\n}\n\n")
            }

            if (doOpen) {
                val prefix = cf.thisType.toJava
                val suffix = if (naive) ".naive-tac.txt" else ".ai-tax.txt"
                Console.println("wrote tac code to: "+writeAndOpen(methodsAsTAC.toString(), prefix, suffix))
            } else {
                Console.println(methodsAsTAC.toString())
            }
        }
    }
}
